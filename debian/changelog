aragorn (1.2.41-3) unstable; urgency=medium

  * Update watchfile to point to working directory again.

 -- Sascha Steinbiss <satta@debian.org>  Tue, 02 Apr 2024 11:14:39 +0200

aragorn (1.2.41-2) unstable; urgency=medium

  * Ensure multiple builds can be done in same directory.
    Done by ensuring the directory is cleaned correctly.
    Closes: #1043665

 -- Sascha Steinbiss <satta@debian.org>  Wed, 15 Nov 2023 11:42:55 +0100

aragorn (1.2.41-1) unstable; urgency=medium

  * Update watchfile.
  * New upstream version.
  * Bump Standards-Version.
  * Remove asciidoctor references.
  * Use watchfile version 4.

 -- Sascha Steinbiss <satta@debian.org>  Sat, 17 Jun 2023 02:29:03 +0200

aragorn (1.2.38-4) unstable; urgency=medium

  * Team upload.
  * Fix FTCBFS: Seed CC from dpkg's buildtools.mk. (Closes: #974635)
    Thank you to Helmut Grohne for the patch.
  * Set CFLAGS, CPPFLAGS, CXXFLAGS, and LDFLAGS via buildflags.mk
  * Bump Standards-Version to 4.5.1

 -- tony mancill <tmancill@debian.org>  Wed, 18 Nov 2020 21:01:36 -0800

aragorn (1.2.38-3) unstable; urgency=medium

  [ Jelmer Vernooĳ ]
  * Team upload
  * Trim trailing whitespace.

  [ Andreas Tille ]
  * Standards-Version: 4.5.0 (routine-update)
  * debhelper-compat 13 (routine-update)
  * Add salsa-ci file (routine-update)
  * Rules-Requires-Root: no (routine-update)
  * Set field Upstream-Contact in debian/copyright.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata (already
    present in machine-readable debian/copyright).

 -- Andreas Tille <tille@debian.org>  Fri, 06 Nov 2020 14:25:25 +0100

aragorn (1.2.38-2) unstable; urgency=medium

  [ Andreas Tille ]
  * Remove homepage field from upstream metadata.

  [ Steffen Moeller ]
  * Update EDAM and upstream metadata.

  [ Sascha Steinbiss ]
  * Use debhelper 11.
  * Bump Standards-Version.
  * Update Vcs-* fields with Salsa addresses.
  * Drop unused patch files.

 -- Sascha Steinbiss <satta@debian.org>  Wed, 04 Jul 2018 18:44:35 +0200

aragorn (1.2.38-1) unstable; urgency=medium

  * New upstream release.
  * Use upstream's manpage instead of building our own.
  * Fix minor spelling issues.

 -- Sascha Steinbiss <satta@debian.org>  Wed, 12 Oct 2016 07:16:41 +0000

aragorn (1.2.37-1) unstable; urgency=medium

  * New upstream release.
  * Drop patch applied by upstream.
  * Update uploader email.
  * Bump Standards-Version.
  * Automate manpage building.
  * Fix URL format in d/copyright.

 -- Sascha Steinbiss <satta@debian.org>  Sun, 03 Jul 2016 07:53:18 +0000

aragorn (1.2.36-6) unstable; urgency=low

  * update Vcs-* to reflect new location on Git

 -- Sascha Steinbiss <sascha@steinbiss.name>  Sun, 07 Feb 2016 08:48:18 +0000

aragorn (1.2.36-5) unstable; urgency=medium

  * Fix freeze with -O2/3.
  * Add simple autopkgtest.
  * Bump standards version.

 -- Sascha Steinbiss <sascha@steinbiss.name>  Tue, 01 Dec 2015 23:32:50 +0000

aragorn (1.2.36-4) unstable; urgency=low

  * Fix hardening flags
  * Add patch description

 -- Sascha Steinbiss <sascha@steinbiss.name>  Wed, 16 Jul 2014 23:29:34 +0100

aragorn (1.2.36-3) unstable; urgency=low

  * hardening-includes => use dpkg-buildflags instead
  * debian/control: Update vcs field
  * debian/control: Update standards version

 -- Sascha Steinbiss <sascha@steinbiss.name>  Wed, 16 Jul 2014 21:56:49 +0100

aragorn (1.2.36-2) unstable; urgency=low

  * Adjust weekday in changelog (fixes lintian warning)
  * Fix spelling error in manpage

 -- Sascha Steinbiss <steinbiss@zbh.uni-hamburg.de>  Tue, 05 Nov 2013 15:22:41 +0100

aragorn (1.2.36-1) unstable; urgency=low

  * Initial release (Closes: #701571)

 -- Sascha Steinbiss <steinbiss@zbh.uni-hamburg.de>  Sat, 16 Feb 2013 10:02:30 +0100
